# Cross-compiling to a Libvirt Windows virtual machine

**This exploratory work-in-progress works (tested on Fedora 33, specifically),
but there are still rough edges and unfinished next steps.**

[[_TOC_]]

## The main goal

This setup guide is intended to make it quick and easy for developers on Linux
to build Firefox for Windows, on Linux, and then run the build directly in a
fast Libvirt virtual machine without even having to copy the build to the
virtual machine. Hopefully this will go some way to mitigating the lack of
Firefox developers on Windows.

![Building Firefox for Windows on Linux, then running it from a shared directory
in a Windows
VM](https://gitlab.com/jwattt/cross-compile-ff-and-libvirt/-/raw/master/cross-compile-and-libvirt.webp)

This setup has several advantages:

1. builds are much faster than on Windows;

2. for many developers it will eliminate the friction of having to maintain a
   separate physical Windows machine for Windows development (Windows builds are
   so slow you really wouldn't want to build in a Windows virtual machine);

3. it eliminates the friction of having separate repositories on separate
   machines and all the sync'ing back and forth during development/review that
   entails;

4. it allows much of the development to be done in the developer's preferred
   environment and with their preferred tools.

## Background

The KVM/QEMU/Libvirt/virt-manager virtualization stack is free, performs very
well (much better than VirtualBox, which is painful) and the recent development
of [virtio-fs](https://virtio-fs.gitlab.io/) (usable as of Fedora 33/Ubuntu
20.10) has recently provided a fast way to share directories with a virtual
machine (what we'll be using here).

In this setup guide we'll be combining Libvirt VMs with a variant of the
cross-compilation process that Mozilla CI uses to cross-compile Firefox for
Windows using Linux builders.

These instructions provide for two ways to set up a Windows VM. You can use
either:

1. the pre-built [Windows 10 development environment virtual
   machines](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/)
   that Microsoft provides specifically for developing Windows applications, or;

2. the [Windows 10 ISO
   installer](https://www.microsoft.com/en-us/software-download/windows10ISO)
   (potentially reusing the Windows product key in your UEFI firmware if your
   computer came with Windows but you replaced it with Linux and aren't
   otherwise using that key?).

### Choosing your Windows setup method

To help in choosing between the two methods it may be helpful to consider the
downsides of each:

Using the Microsoft provided virtual machines has the disadvantage that they
expire every 60 days. That said, once you're familiar with the instructions
here (and have completed the once-off tasks) the provided setup script actually
makes setup relatively easy, and any files you want to keep when you replace a
VM can be stored in the directory Linux shares with the VM. It may also be
worth noting this method involves downloading a 20 GB zip file, extracting a 20
GB disk image, converting that to a 40 GB .qcow2 file, then (with the current
setup) copying that file to the standard Libvirt images directory. So about 120
GB of disk churn every 2 months, if that matters to you.

Installing from the Windows 10 ISO has the disadvantage that you need to go
through the Windows installer manually, you need to have/obtain a valid Windows
license key, and you need to manually download and install Visual Studio
(preinstalled in the Microsoft provided VMs, even if incompletely for our
purposes).

Perhaps start by trying things out with a Microsoft provided VM (easier to set
up) and then if things go well set up a permanent Windows VM.

## Enable hardware virtualization in UEFI/BIOS settings

The first thing you should do is enable hardware virtualization support in your
BIOS/UEFI. How you do that depends on your motherboard and its firmware. If you
have trouble finding the settings look for "VT-x" or "AMD-V".

Once enabled, boot Linux and confirm that it's enabled by running:

```
egrep '^flags.*(vmx|svm)' /proc/cpuinfo
```

Note that QEMU will fall back to much slower software virtualization if
hardware virtualization is not enabled.

## Install packages for Libvirt virtualization

### Fedora install

```
# Use `dnf groupinfo virtualization` for the package list.
sudo dnf install -y @virtualization virt-v2v
```

### Ubuntu install

```
# This should automatically add your user to the libvirt group.
sudo apt install virtinst virt-manager
```

`virt-v2v` is also required, but unfortunately it is no longer part of the
'libguestfs-tools' package (presumably since [virt-v2v was moved to its own
repository](https://github.com/libguestfs/libguestfs/commit/85c99edec19ac7afb38fa6003e35f51db143922c);
[debian bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=966675)). Get it
from the [virt-v2v repository](https://github.com/libguestfs/virt-v2v) (the
setup instructions in the README are trivial) and make sure it's in your PATH so
the setup script that accompanies this guide can find it.

## Optional: Set up persistent huge pages

Huge pages can significantly improve virtual machine memory performance, and of
the two types Linux has - persistent huge pages (HugeTLB) and transparent huge
pages (THP) - the former should offer better performance, albeit at the expense
of having to reserve the RAM upfront when Linux boots.

To provide one data point, running the build output of a fresh build was 15%
slower on Windows without setting up persistent huge pages (I didn't test much
else).

If preferred, this step can be carried out after setting up the virtual machine,
but the virtual machine's XML configuration will also need to be modified (see
the comments in the accompanying setup script).

To set up persistent huge pages, first check your system's huge page size isn't
something unusual that doesn't match the size assumed in this guide and in the
provided script:

```
if ! grep Hugepagesize /proc/meminfo | grep -q " 2048 kB$"; then
  echo "Error: unusual and unexpected hugepage size." >&2
fi
```

If that didn't print an error, run the following **after first adjusting it** to
at least the amount of RAM you want to give your virtual machine:

```
# Amount of RAM for VM in GiB; exported for the VM setup script below.
export GUESTMEMGIB=4

# Add a kernel command line argument to pre-allocate the huge pages from RAM:
# (To revert this, replace `--args` with `--remove-args`. The current number
# of hugepages can be obtain from `cat /proc/cmdline`.)
sudo grubby --update-kernel=ALL --args="hugepagesz=2M hugepages=$(( $GUESTMEMGIB * 1024 / 2 ))"

reboot  # We need the huge pages before running the setup script
```

## Create a directory to share with Windows

This directory should be empty since we'll mount a partition at it below.

```
# Exported to provide the path to the VM setup script below
export SHAREDDIR="$HOME/SharedWithWindowsVM"
mkdir -p "$SHAREDDIR"
```

## Create the filesystem for the shared directory

Skip this section if you're here for some other reason than developing Mozilla
code.

### Background

[Mike Hommey's original post about cross-compiling Firefox for
Windows](https://glandium.org/blog/?p=4020) provides lots of interesting
background for the curious.

For our purposes, to build Firefox for Windows on Linux we'll need a
case-insensitive [capable] filesystem so that we can create a case-insensitive
directory to store the VC++ and Windows SDK files required to cross-compile.

As a fallback option, if you can't make space on your existing internal disk(s)
then you can try out the instructions below to create the filesystem in a .img
file instead.

NOTE: when deciding how big to make the partition/image file, bear in mind that
you'll need enough space for the Mozilla source (say 3G if you're using `hg
share` from another filesystem), an object directory (say 10G) and the Visual
Studio and Windows SDK files we need (say 4G) - so 20G, bare minimum, if you
like living on the edge.

To keep things simple, these instructions will have you mount the
case-insensitive capable filesystem at the directory that we'll be sharing with
Windows. That way we can easily copy the VC++/SDK files directly to the
case-insensitive filesystem from the Windows VM.

### Option 1: Create a new partition

Use your favorite partitioning tool/method - just make sure you set and export
the variable below.

One way to create the partition, assuming you know that there's sufficient
space at the end of `/dev/mydrive`(perhaps by checking the output of `sudo
sfdisk -Fq /dev/mydrive`), might be to append a new partition after the
existing partitions using something like the following. (First run with
`--no-act` (dry-run) and only re-run it without that if the command output
looks good.)

```
sudo sfdisk --no-act --append /dev/mydrive <<< 'name="Shared with Windows VM" size=40GiB'
sudo partprobe /dev/mydrive
```

Whatever method you use, set `NEWBLOCKDEV` to the path of the new partition
(shown in sfdisk's output, if you used the commands above):

```
# Exported for bash invocation below
export NEWBLOCKDEV=/dev/newpartition
```

### Option 2: Create an image file

If you can't free up some disk space, or add another internal disk, or plug a
cheap external USB3 gen 2 NVMe disk into the back of your workstation, then
using an image file instead may work.

Note that if you're using a CoW filesystem, such as BTRFS, you will want CoW
turned off for the image! That should automatically happen for files added to
Libvirt's image directory, which is why we use it in the following commands
(that, and because it will conveniently keep the shared directory's backing .img
file alongside the VM disk image).

```
# TODO: actually test these work and their impact on file sharing perf!

# Either pre-allocate the file without holes (slow to create; best perf in use?):
sudo dd if=/dev/zero of=/var/lib/libvirt/images/shared-with-windows.img \
  status=progress bs=1M count=40960

# Or pre-allocate with holes (fast to create):
sudo fallocate -l 40960M /var/lib/libvirt/images/shared-with-windows.img

# Or create a dynamically allocated (sparse) image file (fast to create,
# minimize disk usage, but will be slowest in use):
sudo truncate --size=40960M /var/lib/libvirt/images/shared-with-windows.img
```

Now set `NEWBLOCKDEV` to the path of the new image file:

```
# Exported for bash invocation below
export NEWBLOCKDEV=/var/lib/libvirt/images/shared-with-windows.img
```

### Option 3-ish: Use liblowercase

CI cross-compilation uses the in-tree `liblowercase` along with a
lowercased-path copy of all the Visual Studio and Windows SDK files. That's fine
if the build will be (optionally packaged up and) copied to Windows' native,
case-insensitive filesystem to run. However, if we want to streamline things so
that we can run the build in Windows directly from the shared directory, then
the liblowercase method isn't ideal, particularly if debug symbols and source
code need to be resolved.

Trying to run the build from a non-case-insensitive filesystem will just pop up
an error dialog with the message:

* "The application has failed to start because its side-by-side configuration
  is incorrect"

In the event that you want to use this method anyway and don't mind losing the
benefits of running the build directly from the shared directory, then start by
creating liblowercase.so by running:

```
mach build build/liblowercase
```

Later, once you've finished creating the MSVS directory below, you'll also need
to create and use a lowercased version of that directory. You can create this
directory using:

```
cd "$SHAREDDIR"
mkdir -p case-lowered-dir
LOWERCASE_DIRS=$PWD/MSVS LD_PRELOAD=/path/to/liblowercase.so cp -r MSVS case-lowered-dir/msvs
```

You'll also need to modify your mozconfig to use liblowercase. Lines to do that
have been included (commented out) in the provided example mozconfig.

If you take this approach then skip the rest of this section.

### Create the filesystem

The script below is provided for your convenience and should format the new
partition as ext4 with the appropriate settings, add it to `/etc/fstab`, and
mount it ready for use.

If the script tells you that your version of Linux doesn't support
case-insensitive ext4 (it was added in [kernel 5.2]
(https://www.man7.org/linux/man-pages/man5/ext4.5.html#KERNEL_SUPPORT)) then
another alternative would be to use VFAT.

Before you run these commands FIRST CHECK THEY MAKE SENSE TO YOU.

```
bash <<EOF
set -e
if [ "$(cat /sys/fs/ext4/features/casefold)" != "supported" ] ; then
  echo "Error: your kernel does not support case-insensitive ext4." >&2
  exit 1
fi
if [ ! -d "$SHAREDDIR" ]; then
  echo "Error: \$SHAREDDIR must be set." >&2
  exit 1
fi
NEWFSUUID="$(uuidgen)"
if [ -f "$NEWBLOCKDEV" ]; then
  FSTABENTRY="$NEWBLOCKDEV $SHAREDDIR  ext4  defaults,noatime,loop  0 0"
else if [ -b "$NEWBLOCKDEV" ]; then
  FSTABENTRY="UUID=$NEWFSUUID $SHAREDDIR  ext4  defaults,noatime  0 0"
else
  echo "Error: \$NEWBLOCKDEV must be set to a partition or an image file." >&2
  exit 1
fi
sudo mkfs.ext4 -O casefold -U "$NEWFSUUID" -E root_owner=$UID:$UID -L shared-to-win-vm $NEWBLOCKDEV
echo "$FSTABENTRY" | sudo tee -a /etc/fstab >/dev/null
sudo systemctl daemon-reload
sudo mount "$SHAREDDIR"
mkdir "$SHAREDDIR/case-insensitive-dir"
chattr +F "$SHAREDDIR/case-insensitive-dir"
[ -d "$SHAREDDIR/lost+found" ] && sudo rmdir "$SHAREDDIR/lost+found"
EOF
```

Note: unfortunately preexisting ext4 filesystems can't have case-insensitivity
support added retroactively using tune2fs (maybe that will change?).

## Run the virtual machine setup script

The accompanying setup script's behavior (for example, virtual machine name, RAM
and CPU core count) can be configured using environment variables. Before you
run the script, read the short configuration section at the top of it.

Specifically, note that by default the script will download one of Microsoft's
Windows app development virtual machines, convert it to a Libvirt compatible
format and then install it as a local Libvirt virtual machine.

If instead you want to set up your own, long-term Windows virtual machine using
the [Windows installer
ISO](https://www.microsoft.com/software-download/windows10) then download the
ISO to a location that is accessible to the 'qemu' user (this probably means to
/var/lib/libvirt/images) and export the variable `WIN10ISOPATH` set to the path
of that .iso file.

Once you're ready, run the script:

```
# This will prompt you for your password for various sudo commands
bash setup-vm.bash
```

If the script completes successfully, open Virtual Machine Manager (virt-manager),
double click your new VM, and power it on.

If you're installing from the Windows Installer ISO - and assuming you have the
legal right to do so - the product key for any old, unused Windows install that
used to be on your physical machine can be obtained via `sudo strings
/sys/firmware/acpi/tables/MSDM`.

### Some notes on deleting virtual machines

Many virtual machine parameters can be changed after the virtual machine has
been created, but if you really want to start afresh for some reason the
following notes on completely deleting Libvirt virtual machines may be useful.

The easiest way to delete a VM is to use virt-manager, via the context menu for
the VM you want to delete in the VM list.

If you want to use the command line, note that, at least on Fedora, the VM's
image(s) normally live in:

  /var/lib/libvirt/images/

and the corresponding XML configuration files in:

  /etc/libvirt/qemu/<vm-name>.xml

However, it's best to use `virsh` to remove VMs rather than manually removing
files in these directories.

First, get a list of the storage targets (images) associated with a VM so that
you can, if you choose, pass them to `--storage` below to have them also be
deleted:

```
sudo virsh domblklist <vmname>
```

The following forces the VM to stop running (we don't use `virsh shutdown` since
that doesn't work without the guest tools being installed), then deletes its
files using:

```
sudo virsh destroy <vmname>
sudo virsh undefine --snapshots-metadata --nvram --storage sda,/path/to/any/extra.qcow2 <vmname>
```

(The --remove-all-storage argument is simpler than the --storage argument, but
note that in the case that you set up the VM using the Windows installer ISO it
will result in the ISO file also being deleted if the ISO is still "connected"
as the "CD" drive to the VM (`virsh domblklist` will show if it is).)

## Initial Windows setup

NOTE: When you first run the VM, you will be prompted by Virtual Machine
Manager to allow it to inhibit (capture) shortcuts. This is particularly
recommended if you will run the Windows VM fullscreen on a second
workspace/virtual desktop, where you'll want things like Ctrl+C, Alt+Tab and
Ctrl+Alt+Del to be handled by Windows.  You can release the shortcut capturing
by tapping Ctrl+Alt to return shortcut handling to Linux.

Open Virtual Machine Manager (virt-manager) and power on the new virtual
machine.

In Windows, download these installers:

* https://github.com/billziss-gh/winfsp/releases/latest
* https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso
  (Once the stable version is 0.1.190 or newer we can stop using 'latest' here.)

Install WinFSP while the .iso is downloading, then once the .iso has finished
downloading open it (double click it, and it will be mounted at an available
drive letter), run 'virtio-win-guest-tools.exe' and install everything.

Now 'View > Scale Display > Auto resize VM with window' in the virtual
machine's menubar should work (it may require a reboot to properly resize).

Tips:
 * run the VM fullscreen on its own desktop workspace
 * remember to tap Ctrl+Alt to release shortcut capturing

## Fix the directory sharing

Currently (with virtio-win tools 0.1.190), the directory sharing doesn't start
properly when Windows boots. This is fixed in the latest, unreleased
development version but for now copy 'Admin-fix-shared-dir.cmd' to the Windows
desktop, then every time Windows starts, right click this file and select "Run
as administrator". Run it now before proceeding to the next section.

## Gather the Windows build dependencies

Building Firefox for Windows requires various files that Visual Studio installs.
Even if you're using one of Microsoft's development virtual machines - which
come with Visual Studio preinstalled - the installer still needs to be rerun to
modify the install to add some extra components.

If you don't have Visual Studio installed, [download the Community
Edition](https://visualstudio.microsoft.com/downloads/) and run the installer.
If you do have it installed, start it, click "Continue without code" at the
splash screen, then from the toolbar select 'Tools' then 'Get Tools and
Features...'. That will rerun the installer - press the "Modify" button.

Install the items listed on the [Install Visual
Studio](https://firefox-source-docs.mozilla.org/setup/windows_build.html#install-visual-studio)
section of the 'Building Firefox On Windows' documentation. The VMs provided by
Microsoft already have the Windows SDK installed. Also "C++ ATL for v142 build
tools" is automatically selected when you select "Desktop development with C++"
so you don't actually need to select that manually.

Additionally install
[LLVM-x.x.x-win64.exe](https://github.com/llvm/llvm-project/releases/latest)
for clang-cl.exe. TODO: This will install 1.5 GB of files when we just want the
one 90 MB file - can we get it more cheaply?

Once everything is installed, gather the requisite files onto the shared
directory by opening a Command Prompt and running something like the following,
**first fixing up the paths and version numbers to match what you have**:

```
# Takes up around 3 GiB of space
set VSDIR="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community"
set DESTDIR="Z:\case-insensitive-dir\MSVS"
xcopy /E /H /K "C:\Program Files (x86)\Windows Kits\10" "%DESTDIR%\WIN10SDK\"
xcopy /E /H /K "%VSDIR%\VC\Tools\MSVC\14.xxx"  "%DESTDIR%\VC-Tools\"
xcopy /E /H /K "%VSDIR%\VC\Redist\MSVC\14.xxx" "%DESTDIR%\VC-Redist\"
xcopy /E /H /K "%VSDIR%\DIA SDK" "%DESTDIR%\DIA-SDK\"
xcopy /E /H /K "C:\Program Files\LLVM\bin\clang-cl.exe" "%DESTDIR%\clang-cl\"
```

Finally, in order for the executables to be found they also need to be marked
executable.  Switch back to Linux, and run something like the following
(failing to do this will later result in "not found" errors trying to run
cl.exe, ml64.exe, midl.exe and fxc.exe):

```
find MSVS -name *.exe | xargs -I{} chmod a+x "{}"
```

## Install cross-compilation dependencies

In Linux, install the following for cross-compiling to Windows:

```
rustup target add x86_64-pc-windows-msvc
```

### Optional: Install the packaging dependencies

The intention of this guide is to allow the build to be run directly from the
shared directory in Windows, without packaging it up first. If you do want to
run `mach package` for some reason then you'll need the following.

On Windows, [install NSIS
3.01](https://sourceforge.net/projects/nsis/files/NSIS 3/3.01/nsis-3.01-setup.exe/download)
(at least that's the version CI uses), copy the installed files to the shared
directory and then modify the variable `MAKENSISU` in your mozconfig to point to
the path of `makensis.exe`.

On Linux, install `7z` or otherwise make sure it's in your PATH.

Fedora:
```
sudo dnf install -y p7zip p7zip-plugins
```

Ubuntu:
```
sudo apt install p7zip-full
```

It doesn't seem to be necessary, but CI also has the `UPX` variable set to the
path of `upx.exe`. If you need that then you can [find a UPX installer
here](https://github.com/upx/upx/releases/latest).

## Build the source

Using the provided `mozconfig` file, build as normal. (The "fixme:" and "err:"
messages that come from Wine seem to be harmless and can be ignored.)

```
./mach build
```

## Run the build on Windows, from the shared directory

Once the build is complete there's one last thing to do. Switching over to the
Windows VM and trying to run `dist/bin/firefox.exe` directly will fail with
errors such as:

* The application has failed to start because its side-by-side configuration is
  incorrect. Please see the application event log or use the command-line
  sxstrace.exe tool for more details.

* Invalid Ms-Dos Function

It turns out this is due to the (heavy) use of symlinks in `dist/bin`, which
Windows can't handle. The simplest way around that is to create a copy of `bin`
with the symlinks replaced with their referents by running the following (using
a shell function/alias to initiate your builds could be prudent):

```
rsync -riL bin/ bin-win    # takes about 1s
```

Running `firefox.exe` from `dist/bin-win` on Windows should then work fine.

## Future improvements

Any volunteers??

* Fix `mach build-backend -b VisualStudio` to generate a solution with Windows
  paths on a best effort basis (e.g. assume the shared drive will be 'Z:')
  since the solution will never be run on Linux. Better yet, allow the user to
  specify a path root.

* Document how to mount the shared directory's "drive" in Windows at a Windows
  directory with an absolute path matching the path on Linux so that debuggers
  on Windows will resolve source files and line numbers for stacks.

* Figure out what is making Wine builds a bit slower than native builds, and
  how to mitigate it. The comments on Mike's post have some insights.

* Figure out if "C++ Clang-cl for v142 build tools (x64/x86)" in the
  "Individual Components" tab of the Visual Studio installer could be useful.
  It didn't seem to install clang-cl.exe though (or even just clang-cl).

* It should be possible to script the install of the Visual Studio components,
  at least for a preexisting install. Note, however, that VS may insist on
  applying the latest updates on its first start before it allows more modules
  to be installed, which could complicate things.

* Could we crib from the function `get_vc_paths` in the Mozilla source to
  automate the fetching of the version numbers in the paths in the script above
  where we the binaries the build needs from Windows to the shared directory?

* Persuade Apple to allow macOS to be (legally) virtualized in Libvirt. :D
