# Troubleshooting

A random collection of issues and fixes.

## Program Error/midl/clang-cl.exe errors

TLDR: use Wine 6.0 instead of Wine 6.3. The addition of `--enable-bootstrap` to
the mozconfig should get provide a working Wine.

Old notes:

When building with Wine version "6.3 (Staging)" (the version Fedora installed),
there were "Program Error" popup dialogs and various midl.exe/clang-cl.exe
errors such as:

* "command line error midl1003 : error returned by the C compiler"
  (clang-cl.exe crashes?)
* "midl : error MIDL9008 : internal compiler problem"

Midl runs clang-cl.exe, and is needed to build the accessibility code's IDL
files. The errors probably only started happening when we started building
those files in parallel:
https://bugzilla.mozilla.org/show_bug.cgi?id=1620133

`--disable-accessibility` might be an option except that:

1. it currently results in build errors in IPDL code
2. the build system still checks for midl anyway:
   https://bugzilla.mozilla.org/show_bug.cgi?id=1620128

One workaround was to run the following before running the main parellelized
build:

```
# You can actually terminate this after reaching the
# accessible/xpcom/xpcAccEvents.h.stub line (at around 50 seconds into a
# clobber build for me) if don't want to wait for the command to finish (which
# takes about 1m 40s for me).
./mach build -j1 export
```

A faster (taking 30s for me) option is the following, but it's harder to
remember, and only builds the majority - not all - of the accessibility IDL
files. (So the parallelized build can't be immediately left unattended after
this command completes.)

```
./mach build -j1 objdir/accessible/interfaces/ia2
```

The following unfortunately doesn't work, partly because `--keep-going` doesn't
work with it, and partly because it seems more likely to fail to avoid the
build errors: (The list of directories comes from searching for "midl.py" in
the moz source.)

```
./mach build -j1 --keep-going objdir/accessible/interfaces/{gecko,ia2,msaa} objdir/accessible/ipc/win/{handler,typelib}
```

TODO: is there some hack we can do to get GeneratedFile to not parallelize
things? What is .force?
https://searchfox.org/mozilla-central/source/mobile/android/base/moz.build#18

