#!/usr/bin/env bash

set -e

# CONFIGURATION VARIABLES
# -----------------------

# Set VMNAME to the name you want to give your virtual machine.
# (A sensible fallback name is set below, possibly based on the .zip name.)
#VMNAME=

# Path to Windows 10 installer .iso - export this before invoking this script
# if you want to install Windows 10 instead of having this script download and
# set up a pre-built Windows app development virtual machine. Download from:
# https://www.microsoft.com/software-download/windows10
# Important: the user 'qemu' must be able to access this file, which probably
# means it should be in /var/lib/libvirt/images
#WIN10ISOPATH=

# Size of disk to create when using the Windows 10 installer (defaults to 40 G)
# TODO: This is ignored for pre-built Windows app development virtual machines
# setup - figure out if virt-v2v can be made to use this, or else try to use
# `virt-resize` or `virsh vol-resize` to do the resizing.
GUESTDISKGIB=${GUESTDISKGIB:-40}

# Size of RAM for virtual machine, in GiB (defaults to 4 GiB)
GUESTMEMGIB=${GUESTMEMGIB:-4}

# Number of CPU cores (defaults to 2)
GUESTCORES=${GUESTCORES:-2}

# Number of CPU threads per CPU core (defaults to 2)
GUESTTHREADSPERCORE=${GUESTTHREADSPERCORE:-2}

# Shared directory
SHAREDDIR=${SHAREDDIR:-$HOME/SharedWithWindowsVM}

# -----------------------


# Check guest disk size looks okay
if [[ ! $GUESTDISKGIB =~ ^[0-9]+$ ]] || [ $GUESTDISKGIB -lt 10 -o $GUESTDISKGIB -gt 1000 ]; then
  echo "Error: \$GUESTDISKGIB must be set to a valid integer (unit GiB)" >&2
  exit 1
fi

# Check guest memory looks okay
if [[ ! $GUESTMEMGIB =~ ^[0-9]+$ ]] || [ $GUESTMEMGIB -lt 2 -o $GUESTMEMGIB -gt 16 ]; then
  echo "Error: \$GUESTMEMGIB must be set to a valid integer (unit GiB)" >&2
  exit 1
fi

GUESTMEMKIB=$(( $GUESTMEMGIB * 1024 * 1024 ))

# Check the hugepage size isn't something unnusual
if ! grep Hugepagesize /proc/meminfo | grep -q " 2048 kB$"; then
  # TODO use `awk '{print $2, $3}'` to get the size and unit into env variables
  echo "Error: unnusual and unexpected hugepage size." >&2
  echo "The --memorybacking line below will need to be fixed." >&2
  exit 1
fi

# Check the shared directory exists
if [ ! -d "$SHAREDDIR" ]; then
  echo "Error: \$SHAREDDIR must be set to a directory that exists." >&2
  echo "Error: currently \$SHAREDDIR='$SHAREDDIR'" >&2
  exit 1
fi

# And is empty, or already set up
if [ ! -z "$(ls -A $SHAREDDIR)" ]; then
  MOUNTSRC="$( findmnt $SHAREDDIR -o source -n || : )"
  if [ -z "$MOUNTSRC" ] || ! sudo dumpe2fs -h "$MOUNTSRC" 2>&1 | grep -q casefold; then
    echo "Error: \$SHAREDDIR ($SHAREDDIR) should be empty or case-insensitive capable" >&2
    exit 1
  fi
fi


check_vm_name () {
  if sudo virsh list --all --name | grep -q "^$1$"; then
    echo "Error: a virtual machine named '$1' already exits." >&2
    echo "Set the environment variable 'VMNAME' to a different name and try again." >&2
    echo "You can get a list of all the current VMs using 'sudo virsh list --all'." >&2
    exit 1
  fi
}


if [ ! -z "$WIN10ISOPATH" ]; then
  if [ ! -f "$WIN10ISOPATH" ] || [[ "$WIN10ISOPATH" != *.iso ]]; then
    echo "Error: \$WIN10ISOPATH ($WIN10ISOPATH) must be a valid .iso file" >&2
    exit 1
  fi
  if ! sudo -u qemu test -r "$WIN10ISOPATH"; then
    echo "Error: user 'qemu' cannot access the file that \$WIN10ISOPATH points to" >&2
    echo "Try moving the file to /var/lib/libvirt/images, updating the variable and re-running" >&2
    exit 1
  fi
  VMNAME=${VMNAME:-Windows10}
  check_vm_name "$VMNAME"
  CDROMARG="--cdrom $WIN10ISOPATH"
  DISKARG="--disk size=$GUESTDISKGIB,sparse=yes,driver.discard=unmap"
else
  # Setting up a pre-built Windows app development virtual machine

  # Resolve the HTTP redirects for the VM in advance (to get the zip's filename):
  VMURL=$(curl "https://aka.ms/windev_VM_virtualbox" -s -L -I -o /dev/null -w '%{url_effective}')
  if [[ "$VMURL" != *.zip ]]; then
    echo "Failed to resolve VM URL" >&2
    exit 1
  fi

  ZIPNAME=$(basename "$VMURL")
  EXTRACTDIR="${ZIPNAME%.*}"
  # Strips the ".VMware"/".VirtualBox" suffix:
  ZIPNAMEBASE="${EXTRACTDIR%.*}"
  if [ -z ${VMNAME+x} ]; then
    VMNAME="$ZIPNAMEBASE"
  fi
  check_vm_name "$VMNAME"

  if [[ "$EXTRACTDIR" == *".VirtualBox" ]]; then
    OVASRC="$EXTRACTDIR/$VMNAME.ova"
  elif [[ "$EXTRACTDIR" == *".VMware" ]]; then
    # The .VMware zip file has the OVA as a separate .vmdk, .mf and .ovf file.
    OVASRC="$EXTRACTDIR"
  else
    echo "Error: unknown virtual machine type." >&2
    echo "Error: Currently only VMware and VirtualBox are handled." >&2
    exit 1
  fi

  # Check $VMNAME


  echo "Downloading $VMURL"

  # Using '-C -' here to continue any interupted download, and to avoid
  # redownloading if the zip exists locally.
  # XXX: TODO: add -f once the patch for this issue makes it to the main distros:
  # https://github.com/curl/curl/issues/6740
  if curl -O -C - --retry 100 "$VMURL"; then
    echo "Download complete"
  else
    echo "Download failed" >&2
    exit 1
  fi


  echo "Extracting '$ZIPNAME' to '$EXTRACTDIR/' (skipping pre-existing files)"

  if ! unzip -n "$ZIPNAME" -d "$EXTRACTDIR"; then
    echo "Failed to extract $ZIPNAME" >&2
    rm -rf "$EXTRACTDIR"
    exit 1
  fi


  echo "Converting image to .qcow2 format"

  CONVERTEDDIR="$EXTRACTDIR-to-libvirt"
  mkdir "$CONVERTEDDIR"

  # NOTE: Unfortunately virt-v2v does not like to be invoked using sudo.
  # See https://listman.redhat.com/archives/libguestfs/2017-April/msg00100.html
  # (I haven't tried to understand/use the LIBGUESTFS_BACKEND=backend thing yet.)
  # That prevents us directly generating the .qcow2 file in the Libvirt
  # images directory, which might be preferable to avoid copying the .qcow2
  # file below.
  # NOTE: we don't use the .xml file that virt-v2v outputs (does it come from the
  # input to virt-v2v or is it a generic config virt-v2v creates?), since we use
  # virt-install below to create our own with our own configuration.
  # NOTE: See the comment on using '-o virtio' at the end of this file.
  virt-v2v -i ova "$OVASRC" -o local -of qcow2 -on "$VMNAME" -os "$CONVERTEDDIR"
  cd "$CONVERTEDDIR"
  mv "$VMNAME-sda" "$VMNAME.qcow2"
  mv "$VMNAME.xml" "$VMNAME.ignored-original.xml"


  echo "Copying '$VMNAME.qcow2' to '/var/lib/libvirt/images'"

  # qemu doesn't have permission to access the directory that we
  # generated the .qcow2 file in, so we can't pass
  # `--disk /path/to/$VMNAME.qcow2` below and have virt-install copy it
  # to the libvirt images directory (even when run as root). Instead we
  # copy the .qcow2 file to the libvirt images directory manually and
  # then pass '--import' along with '--disk' when invoking virt-install
  # to stop it from creating its own copy of that .qcow2 file.
  # (FWIW hardlinking the .qcow2 file isn't possible for me since both
  # /home and /var/lib/libvirt/images are separate BTRFS filesystems
  # on my setup. Symlinking also fails for some reason, with virt-install
  # complaining that the .qcow2 file is a "non-existant file"...)

  sudo cp "$VMNAME.qcow2" /var/lib/libvirt/images


  DISKARG="--import --disk /var/lib/libvirt/images/$VMNAME.qcow2"
fi


echo "Creating libvirt virtual machine '$VMNAME'"

HUGEPAGECOUNT=$(cat /proc/meminfo | grep HugePages_Total | awk '{print $2}')
HUGEPAGESETTINGS=""
if [ $HUGEPAGECOUNT -ge $(( GUESTMEMGIB * 1024 / 2 )) ]; then
  # Users can retrospectively have the new virtual machine use persistent huge
  # pages by adding the following to the <memoryBacking> element in the XML
  # configuration (use virt-manager or `virsh edit` - do not edit manually):
  # <hugepages><page size="2048" unit="KiB"/></hugepages>
  HUGEPAGECONFIG="hugepages.page0.size=2048,hugepages.page0.unit=KiB,"
fi

# The '--import' is necessary to stop virt-install copying the .qcow2.
# The '--noreboot' stops virt-install from running the new VM when it
# opens virt-viewer (I'd rather it didn't even do that!) so that we can do the
# first run in virt-manager instead.
# The 'access.mode=shared' is required for virtio-fs.
# Using '--graphics spice --video qxl' seems to be necessary to get
# 'Auto resize VM with window' to work (after the guest tools are
# installed).
# The 'model.vgamem=32768' increases the vgamem high enough to allow
# 'Auto resize VM with window' to work with a 4K display.
# Must run using sudo, or else it will fail with:
#   ERROR with older libvirtd 6.6.0 and virt-install 3.1.0: "unsupported
#   configuration: virtiofs is not yet supported in session mode".
# With QEMU 5.0.0+ and libvirt 6.9.0+, virtio-fs will no longer require the
# complicated NUMA setup, according to https://libvirt.org/kbase/virtiofs.html
GUESTCPUTHREADS=$(($GUESTCORES * $GUESTTHREADSPERCORE))
sudo virt-install \
  --name $VMNAME \
  --os-variant detect=on,name=win10 \
  --memory $GUESTMEMKIB \
  --vcpus $GUESTCPUTHREADS \
  --cpu host-passthrough,cache.mode=passthrough,topology.sockets=1,topology.cores=$GUESTCORES,topology.threads=$GUESTTHREADSPERCORE,numa.cell0.id=0,numa.cell0.cpus=0-$(($GUESTCPUTHREADS - 1)),numa.cell0.memory=$GUESTMEMKIB,numa.cell0.memAccess=shared \
  --memorybacking ${HUGEPAGECONFIG}access.mode=shared \
  --graphics spice \
  --video qxl,model.vgamem=32768 \
  $CDROMARG \
  $DISKARG \
  --filesystem type=mount,accessmode=passthrough,driver.type=virtiofs,source.dir=$SHAREDDIR,target.dir=SharedFromLinux \
  --noreboot \
  &

VIRTINSTPID=$!


# This kills virt-viewer (harmless, so long as --noreboot is used above) so
# that we can do the initial run in virt-manager, which we'll want to use to
# manage/run VMs in general anyway.
TRIES=60
until [ $TRIES -eq 0 ] || sudo kill $(ps -a | grep virt-viewer | awk '{print $1}') &>/dev/null; do
  sleep 0.1
  : $(( TRIES-- ))
done

if ! wait $VIRTINSTPID; then
  # virt-install failed
  exit 1
fi


sleep 1

echo ""
echo "Done - now open virt-manager and power on '$VMNAME'."
echo "Once Windows starts/finishes installing, install:"
echo "  https://github.com/billziss-gh/winfsp/releases/latest"
echo "then fetch and open (e.g. double click):"
# TODO: once the stable version is 0.1.190 or newer we can stop using 'latest' here.
echo "  https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso"
echo "then run its 'virtio-win-guest-tools.exe' and install everything."
# TODO: add service restart message
echo "That done, 'View > Scale Display > Auto resize VM with window' in the VM menubar should work."
echo "Tip - I suggest you run the VM fullscreen on its own desktop workspace."
echo "Tip - the VM will capture all keyboard events - tap Ctrl+Alt to release."

#echo "Reminder: change the Windows keyboard layout via:"
#echo "Settings > Time & Language > Language > Preferred languagel section > select current > click Options button > Add a keyboard"

exit 0




# Extra/old notes:


# One thing I tried was using virt-install with `--print-xml --dry-run` to
# create a separate .xml file, then copying that file and the .qcow2 into
# their respective libvirt directories. However, it seems that if
# virt-install doesn't know about the actual .qcow2 file at the time it is
# invoked, the resulting .xml file's configuration is incompatible in
# various ways.
#
# When trying to get the `virt-install --print-xml --dry-run` approach to
# work, it seemed desirable to avoid copying too many times. To do that
# it requires that the 'qemu' user has access to the .qcow2 file (so not
# under $HOME) and that the file be created on the same file system/mount
# point as /var/lib/libvirt/images. That rules out the following since they
# are all mount points:
#
#   /boot
#   /dev
#   /home
#   /proc
#   /root
#   /run
#   /sys
#   /tmp
#
# The following are all under the same mount as /var/lib/libvirt/images:
#
#   /media
#   /mnt
#   /opt
#   /srv
#   /usr
#   /var
#
# Of those, the only place that seems to make sense to output the .qcow2 
# file seems to be /var/tmp (especially for Silverblue).
#
# Also note that to match the existing permissions, if the above approach
# was to be used then you should also run:
# `sudo chmod 600 "/etc/libvirt/qemu/$VMNAME.xml"`
# `sudo chown root:root /var/lib/libvirt/images/$VMNAME.qcow2
#
# And take care that the following line in the .xml file points to the
# .qcow2 file's final location:
#   <source file='/var/lib/libvirt/images/VMNAME.qcow2'/


# Example error when trying to use `-o libvirt` to write the .qcow2 file
# directly to /var/lib/libvirt/images :
#
# $ sudo virt-v2v -i ova "$EXTRACTDIR" -o libvirt -of qcow2 -on "$VMNAME"
# [   0.0] Opening the source -i ova .
# virt-v2v: warning: making OVA directory public readable to work around 
# libvirt bug https://bugzilla.redhat.com/1045069
# [  37.4] Creating an overlay to protect the source from being modified
# [  37.5] Opening the overlay
# virt-v2v: error: libguestfs error: could not create appliance through 
# libvirt.
#
# Try running qemu directly without libvirt using this environment variable:
# export LIBGUESTFS_BACKEND=direct
#
# Original error from libvirt: Cannot access backing file 
# '/home/jwatt/Downloads/WinDev2102Eval.VMware/WinDev2102Eval-disk1.vmdk' of 
# storage file '/var/tmp/v2vovl54dc10.qcow2' (as uid:107, gid:107): 
# Permission denied [code=38 int1=13]
#
# If reporting bugs, run virt-v2v with debugging enabled and include the 
# complete output:
#
#   virt-v2v -v -x [...]

