# The directory that libvirt is configured to share with the Windows VM.
SHAREDDIR="$HOME/SharedWithWindowsVM"


ac_add_options --target=x86_64-pc-mingw32

# Automatically bootstrap toolchains to get `wine64`.
ac_add_options --enable-bootstrap

ac_add_options --enable-debug-symbols --disable-optimize --disable-crashreporter


# The objdir needs to be in the case-insensitive directory for Windows to be
# able to run the build output directly.
mk_add_options MOZ_OBJDIR=$SHAREDDIR/case-insensitive-dir/objdir/$(basename $PWD)-no-opt

# Or else use the liblowercase method, but then Windows won't be able to run
# the resulting objdir files without you copying them, out-of-band, to Windows'
# own filesystem (there are "Can't read from the source file or disk." errors
# for some files when trying to copy them from the shared directory in Windows,
# which is presumably a virtio-fs issue):
#mk_add_options MOZ_OBJDIR=$SHAREDDIR/case-sensitive-dir/objdir/$(basename $PWD)-no-opt
#mk_add_options "export LD_PRELOAD=$SHAREDDIR/liblowercase/liblowercase.so"
#mk_add_options "export LOWERCASE_DIRS=$SHAREDDIR/case-lowered-dir"


export VSPATH="$SHAREDDIR/case-insensitive-dir/MSVS"
# Or if using the liblowercase method:
#export VSPATH="$SHAREDDIR/case-lowered-dir/msvs"
export VSWINPATH="$VSPATH"
export DIA_SDK_PATH="$VSPATH/DIA_SDK"
export WINDOWSSDKDIR="$VSPATH/WIN10SDK"
export WIN32_REDIST_DIR="$VSPATH/VC-Redist/x64/Microsoft.VC142.CRT"
export WIN_UCRT_REDIST_DIR="$WINDOWSSDKDIR/Redist/ucrt/DLLs/x64"
# Copied from "C:\Program Files\LLVM" after installing the .exe from:
# from https://github.com/llvm/llvm-project/releases/latest
#export LIB="$VSPATH/LLVM/lib/clang/11.1.0/lib/windows"

# For cl.exe and ml64.exe etc.
export PATH="$PATH:$VSPATH/VC-Tools/bin/Hostx64/x64"


# So midl.exe can find clang-cl.exe:
mk_add_options "export PATH=$PATH:$SHAREDDIR/clang-cl"

# Failed, incomplete attempt to see if replacing `midl.exe` with `widl` would
# make building the accessibility code less prone to build errors (it was much
# worse):
#export MIDL="/usr/bin/widl"
#export MIDL_FLAGS="-I /usr/include"
# or:
#export MIDL="$HOME/Downloads/wine/bin/widl"
#export MIDL_FLAGS="-I $HOME/Downloads/wine/include -I $WINDOWSSDKDIR/include/10.0.18362.0/um -I $WINDOWSSDKDIR/include/10.0.18362.0/shared"


# Get `mach package` working:
export MAKENSISU=$HOME/Downloads/nsis-3.01/makensis.exe
# Disabled due to error about "maintenanceservice_installer.exe" being missing:
ac_add_options --disable-maintenance-service
# But then that causes "undeclared identifier 'IsSecureUpdateStatusSucceeded'",
# so to avoid that too:
ac_add_options --disable-updater


# CI does this, but it doesn't seem to be necessary:
# (The `clang` dir being fetched using the URL in one of the CI build's logs.)
#export CC=$HOME/Downloads/clang/bin/clang-cl
#export CXX=$HOME/Downloads/clang/bin/clang-cl
#export HOST_CC=$HOME/Downloads/clang/bin/clang
#export HOST_CXX=$HOME/Downloads/clang/bin/clang++
#export ENABLE_CLANG_PLUGIN=1
#export LINKER=lld-link

